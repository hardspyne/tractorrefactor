# Tractor Refactor
____
### installation requirements:
  - JDK 8
  - Maven
  
### How to start:
 1. Open command line in the project directory and enter:
    - 'mvn package'
    - 'java -jar target/tractor-refactor-0.1.0.jar'
