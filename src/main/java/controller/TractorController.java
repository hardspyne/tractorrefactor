package controller;

import model.Direction;
import model.Tractor;
import view.Field;

import java.awt.event.KeyEvent;

public class TractorController {
    private int sizeX;
    private int sizeY;

    public static void main(String[] args) {
        TractorController tractorController = new TractorController();
        tractorController.run();
    }

    private void run() {
        sizeX = 10;
        sizeY = 10;

        KeyboardObserver keyboardObserver = new KeyboardObserver();
        keyboardObserver.start();

        Field field = new Field(sizeX, sizeY);
        Tractor tractor = new Tractor(sizeX, sizeY);


        while (tractor.isAlive()) {

            if (keyboardObserver.hasKeyEvents()) {
                KeyEvent event = keyboardObserver.getEventFromTop();
                switch (event.getKeyCode()) {
                    case KeyEvent.VK_LEFT:
                        tractor.move(Direction.LEFT);
                        break;
                    case KeyEvent.VK_RIGHT:
                        tractor.move(Direction.RIGHT);
                        break;
                    case KeyEvent.VK_UP:
                        tractor.move(Direction.UP);
                        break;
                    case KeyEvent.VK_DOWN:
                        tractor.move(Direction.DOWN);
                        break;
                }
            }

            field.print(tractor.getPositionX(), tractor.getPositionY());
            sleep();
        }
    }

    private void sleep() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public int getSizeX() {
        return sizeX;
    }

    public void setSizeX(int sizeX) {
        this.sizeX = sizeX;
    }

    public int getSizeY() {
        return sizeY;
    }

    public void setSizeY(int sizeY) {
        this.sizeY = sizeY;
    }
}
