package model;

import exceptions.TractorInDitchException;

public class Tractor {

    private int[] position;

    private boolean isAlive;

    private int sizeX;
    private int sizeY;

    public Tractor(int sizeX, int sizeY) {
        this.sizeX = sizeX;
        this.sizeY = sizeY;

        position = new int[]{sizeX / 2, sizeY / 2};

        isAlive = true;
    }

    public void move(Direction direction) {
        switch (direction) {
            case UP:
                position = new int[]{position[0], position[1] - 1};
                break;
            case RIGHT:
                position = new int[]{position[0] + 1, position[1]};
                break;
            case DOWN:
                position = new int[]{position[0], position[1] + 1};
                break;
            case LEFT:
                position = new int[]{position[0] - 1, position[1]};
                break;
        }

        if (position[0] > sizeX || position[1] > sizeY
                || position[0] < 0 || position[1] < 0) {

            try {
                throw new TractorInDitchException("Out of border");
            } catch (TractorInDitchException e) {
                e.printStackTrace();
                isAlive = false;
            }

        }
    }

    public int getPositionX() {
        return position[0];
    }

    public int getPositionY() {
        return position[1];
    }

    public boolean isAlive() {
        return isAlive;
    }

    public void setAlive(boolean alive) {
        isAlive = alive;
    }
}