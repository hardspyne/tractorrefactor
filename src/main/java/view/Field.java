package view;

public class Field {

    private int sizeX;
    private int sizeY;

    private int[] field;

    public Field(int sizeX, int sizeY) {
        this.sizeY = sizeY;
        this.sizeX = sizeX;

        field = new int[]{sizeX, sizeY};
    }

    public int getSizeX() {
        return sizeX;
    }

    public int getSizeY() {
        return sizeY;
    }

    public int[] getField() {
        return field;
    }


    public void print(int tractorPostionX, int tractorPositionY) {
        for (int i = 0; i < sizeY; i++) {
            for (int j = 0; j < sizeX; j++) {
                if (tractorPostionX == j && tractorPositionY == i) {
                    System.out.print("\uD83D\uDE9C");
                } else {
                    System.out.print("_");
                }
            }
            System.out.println();
        }

        System.out.println("\n");
    }

}
