package exceptions;

public class TractorInDitchException extends Exception {

    public TractorInDitchException() {
        super();
    }

    public TractorInDitchException(String message) {
        super(message);
    }

    public TractorInDitchException(String message, Throwable cause) {
        super(message, cause);
    }

    public TractorInDitchException(Throwable cause) {
        super(cause);
    }

    public TractorInDitchException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}